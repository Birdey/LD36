﻿// Made by Hufo (Twitch hufo_)

using UnityEngine;
using System;
using System.Collections;

public class SnowManager : MonoBehaviour
{

    public static SnowManager me;

    float _SnowGlobalOffset = 0.0f;
    /// <summary>
    /// Snow noise offset, automatically reset everytime the snow melts completely (so that patterns are different every time)
    /// </summary>
    public float SnowGlobalOffset
    {
        get { return _SnowGlobalOffset; }
        set
        {
            if (value != _SnowGlobalOffset)
            {
                _SnowGlobalOffset = value;
                Shader.SetGlobalFloat("_SnowGlobalOffset", value);
            }
        }

    }

    float _SnowGlobalFactor = -1.0f;
    /// <summary>
    /// Current snow factor (0 = no snow, 1 = full snow)
    /// </summary>
    public float SnowGlobalFactor
    {
        get { return _SnowGlobalFactor; }
        set
        {
            if (value != _SnowGlobalFactor)
            {
                _SnowGlobalFactor = value;
                Shader.SetGlobalFloat("_SnowGlobalFactor", value);
                if (_SnowGlobalFactor == 0.0)
                { // snow disappeared -> randomize offset to show a different pattern next time
                    SnowGlobalOffset = UnityEngine.Random.Range(-1000.0f, 1000.0f);
                }
            }
        }
    }

    public float TargetSnowFactorSpring = 0.0f;
    public float TargetSnowFactorSummer = 0.0f;
    public float TargetSnowFactorAutumn = 0.3f;
    public float TargetSnowFactorWinter = 0.8f;

    public float TargetSnowFactor = 0.0f;
    public bool isSunny = false;
    public bool isSnowing = false;
    public float SnowFallingRate = 0.15f;
    public float SnowMeltingRate = 0.25f;

    // Use this for initialization
    void Start ()
    {
        // reset snow, random offset
        SnowGlobalFactor = 0.0f;
    }
	
	// Update is called once per frame
	void Update ()
    {
        // this code is used to see the snow independently of the seasons and weather logic.
        //++tickCount;
        //SnowGlobalFactor = Math.Max(0.0f, (float)Math.Sin(-tickCount / 400.0));

        // this version wire it up to the seasons and weather (day/night)
        float factor = SnowGlobalFactor;
        float delta = 0.0f;
        if (TargetSnowFactor > factor)
        {
            if (isSnowing)
                delta = SnowFallingRate;
        }
        else if (TargetSnowFactor < factor)
        {
            if (isSunny) delta = -SnowMeltingRate;
        }
        delta *= Time.deltaTime;
        if (Math.Abs(delta) >= Math.Abs(TargetSnowFactor-factor))
        {
            factor = TargetSnowFactor;
        }
        else
        {
            factor += delta;
        }
        SnowGlobalFactor = factor;
    }

    #region Events
    void OnEnable()
    {
        TickManager.workTick += GetTick;
    }

    void OnDisable()
    {
        TickManager.workTick -= GetTick;
    }

    void GetTick()
    {
        // link the biggest heat source to the shader variables
        if (TemperatureManager.me)
        {
            HeatSource hs = TemperatureManager.me.GetStrongestHeatSource();
            if (hs)
            {
                Shader.SetGlobalVector("_SnowGlobalHeatSource", hs.transform.position);
                float range = Math.Min(hs.heatRange, hs.heatGeneration * 2.0f);
                Shader.SetGlobalFloat("_SnowGlobalHeatRangeInvSqr", 1.0f / (range * range));
            }
        }
    }

    void OnDayTimeChanged(TimeOfDay _dayTime, Season _season)
    {
        // todo: weather conditions
        isSunny = (_dayTime != TimeOfDay.Night);
        isSnowing = true;
    }
    void OnSeasonChanged(TimeOfDay _dayTime, Season _season)
    {
        switch(_season)
        {
            case Season.Spring: TargetSnowFactor = TargetSnowFactorSpring; break;
            case Season.Summer: TargetSnowFactor = TargetSnowFactorSummer; break;
            case Season.Autumn: TargetSnowFactor = TargetSnowFactorAutumn; break;
            case Season.Winter: TargetSnowFactor = TargetSnowFactorWinter; break;
        }
    }
    #endregion

    void Awake()
    {
        me = this;
        TimeManager.onDayTimeChanged += OnDayTimeChanged;
        TimeManager.onSeasonChanged += OnSeasonChanged;
    }

}
