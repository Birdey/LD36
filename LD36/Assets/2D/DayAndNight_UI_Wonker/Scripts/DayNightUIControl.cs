﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Script controls Day'n'Night Season UI
/// created by Wonker21
/// </summary>
public class DayNightUIControl : MonoBehaviour {
    public Color autumnCol;
    [Header("Text")]
    public Text dayCounter;

    public Color eveningCol;
    //Tint objects based on current season
    [Header("Tint Objects")]
    public Image[] ground;
    public Color middayCol;
    [Header("Time of Day Sky Colors")]
    public Color morningCol;
    public Color nightCol;
    //Object which holds sun and moon
    [Header("Rotator")]
    public RectTransform rotator;
    public Image sky;

    [Header("Season Ground Colors")]
    public Color springCol;
    public Color summerCol;
    public Color winterCol;
    //Angles
    float[] angles = new float[4] { 0f, -90f, -180f, -270f };

    public void UpdateDayCounterUI(int _day) {
        dayCounter.text = _day.ToString();
    }

    IEnumerator ColorTo(Image applyTo, Color col) {
        Color from = applyTo.color;
        Color to = col;
        WaitForEndOfFrame waitTime = new WaitForEndOfFrame();
        float duration = 1f;
        float t = duration;
        while(t > 0f) {
            applyTo.color = Color.Lerp(from, to, duration - t);
            t -= Time.deltaTime;
            yield return waitTime;
        }
        applyTo.color = to;
        yield return null;
    }

    IEnumerator RotateTo(TimeOfDay _dayTime) {
        //get angle based on ENUM value
        float angle = angles[(int)_dayTime];

        Quaternion from = rotator.rotation;
        Quaternion to = Quaternion.Euler(0f, 0f, angle);
        WaitForEndOfFrame waitTime = new WaitForEndOfFrame();
        float duration = 1f;
        float t = duration;
        while(t > 0f) {
            rotator.rotation = Quaternion.Slerp(from, to, duration - t);
            t -= Time.deltaTime;
            yield return waitTime;
        }
        rotator.rotation = to;
        yield return null;
    }

    void Start() {
        //subscribe to events
        TimeManager.onDayTimeChanged += UpdateTimeofDay;
        TimeManager.onSeasonChanged += UpdateSeason;

        //update for the first time
        TimeManager temp = Object.FindObjectOfType<TimeManager>();
        UpdateSeason(temp.curTimeOfDay, temp.curSeason);
    }

    void TintGround(Color col) {
        foreach(Image img in ground) {
            StartCoroutine(ColorTo(img, col));
        }
    }

    void UpdateSeason(TimeOfDay _dayTime, Season _season) {
        //change colors of the ground
        switch(_season) {
        case Season.Spring:
            TintGround(springCol);
            break;
        case Season.Summer:
            TintGround(summerCol);
            break;
        case Season.Autumn:
            TintGround(autumnCol);
            break;
        case Season.Winter:
            TintGround(winterCol);
            break;
        default:
            break;
        }
    }

    void UpdateTimeofDay(TimeOfDay _dayTime, Season _season) {
        UpdateDayCounterUI(TimeManager.curDay);
        //change color of the sky
        switch(_dayTime) {
        case TimeOfDay.Morning:
            StartCoroutine(ColorTo(sky, morningCol));
            break;
        case TimeOfDay.Midday:
            StartCoroutine(ColorTo(sky, middayCol));
            break;
        case TimeOfDay.Evening:
            StartCoroutine(ColorTo(sky, eveningCol));
            break;
        case TimeOfDay.Night:
            StartCoroutine(ColorTo(sky, nightCol));
            break;
        default:
            break;
        }
        StartCoroutine(RotateTo(_dayTime));
    }
}