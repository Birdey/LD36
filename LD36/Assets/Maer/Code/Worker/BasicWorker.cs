﻿using UnityEngine;
using System.Collections;

public class BasicWorker : MonoBehaviour
{
    #region Variables
    [Header("Harvesting")]
    public int harvestStrengh = 1;
    public int harvestDistance = 3;

    [Header("Heat")]
    public int heat;

    [Header("Referenzes")]
    public Ressource curRessource;
    public NavMeshAgent agent;
    #endregion

    float scared = 0;

    #region Event
    void OnEnable()
    {
        TickManager.workTick += GetTick;
    }

    void OnDisable()
    {
        TickManager.workTick -= GetTick;
    }

    void Update()
    {
        if (scared > 0)
            scared -= Time.deltaTime;
        else if (scared < 0) scared = 0;
    }

    public void GetTick()
    {
        if (curRessource == null)
        {
            curRessource = RessourceManager.me.GetRessource();

            if (curRessource != null)
            {
                curRessource.curWorker = this;
                MoveToRessource();
            }
        }
        else
        {
            if (GetDistance() <= harvestDistance)
            {
                if (!Frighten(curRessource))
                    curRessource.harvest(harvestStrengh);
                else
                {
                    Ressource res = curRessource;
                    RessourceHarvestet();
                    res.Unhighlight();
                }
            }
                
        }
    }

    bool Frighten(Ressource resource)
    {
        if (scared != 0)
            return true;

        Ghost ghost = resource.GetComponentInChildren<Ghost>();

        if (ghost != null)
        {
            agent.SetDestination(ghost.Boo(transform.position));
            Debug.Log("Scared!");
            scared = ghost.scareTime;
            return true;
        }

        return false;
    }
    #endregion

    void Start()
    {
        TemperatureManager.me.AddWorker(this);
    }

    #region Ressource harvesting
    private void MoveToRessource()
    {
        agent.SetDestination(curRessource.transform.position);
    }

    public void RessourceHarvestet()
    {
        
        curRessource = null;
    }

    float GetDistance()
    {
        return Vector3.Distance(this.transform.position, curRessource.transform.position);
    }
    #endregion

    #region Heat
    void GetHeated(int _heat)
    {
        heat = _heat;
    }
    #endregion
}
