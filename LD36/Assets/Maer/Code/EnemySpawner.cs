﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour {

    [Header("Spawn Interval")]
    private float curtimer;
    public float spawnTimeMin = 30;
    public float spawnTimeMax = 60;
    public float unitInterval = 0.5f;

    [Header("Spawn Strengh")]
    public int waveCount = 0;
    public int minEnemies = 1;
    public int maxEnemies = 3;
    private static int curEnemyAmount = 0;
    private int spawnedEnemyAmount = 0;
    private int targetMobAmount = 0;

    [Header("Referenzes")]
    public GameObject enemyPrefab;
    public Transform[] spawnPositions;
    public Text timeDisplay;

    void Awake()
    {
        curtimer = Random.Range(spawnTimeMin, spawnTimeMax);
    }

    void Update()
    {
        if(curEnemyAmount <= 0)
        {
            Timer();
        }
    }

    public void Timer()
    {  
        if(curtimer <= 0)
        {
            StartSpawningEnemys();
            curtimer = Random.Range(spawnTimeMin, spawnTimeMax);
        }
        else
        {
            curtimer -= Time.deltaTime;

        }

        timeDisplay.text = "" + (int)curtimer;
    }

    void StartSpawningEnemys()
    {
        spawnedEnemyAmount = 0;
        targetMobAmount = (Random.Range(minEnemies, maxEnemies)) + waveCount;

        waveCount++;

        SpawnEnemy();
    }

    void SpawnEnemy()
    {
        if (spawnedEnemyAmount >= targetMobAmount)
            return;

        GameObject go = Instantiate(enemyPrefab, spawnPositions[Random.Range(0, spawnPositions.Length)].position, Quaternion.identity) as GameObject;
        spawnedEnemyAmount++;
        StartCoroutine(WaitAndSpawn());
    }

    public static void AddEnemy()
    {
        curEnemyAmount++;
    }

    public static void RemoveEnemy()
    {
        curEnemyAmount--;
    }

    IEnumerator WaitAndSpawn()
    {
        yield return new WaitForSeconds(unitInterval);
        SpawnEnemy();
    }
}
