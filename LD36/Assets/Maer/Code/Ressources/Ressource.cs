﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Ressource : MonoBehaviour {

    #region Audio
    public AudioClip harvestSound;
    public float harvestSoundVolume;
    GameObject currentPlayingSound;
    DateTime lastTimeAudioStarted;
    #endregion Audio

    public int curHitpoints = 10;
    public BasicWorker curWorker;
    public GameObject itemDrop;
    public int maxHitpoints = 10;
    public GameObject particle;
    private bool addedToList = false;
    GameObject highlight;

    public void harvest(int _strengh) {
        if(DateTime.Now - lastTimeAudioStarted > TimeSpan.FromSeconds(harvestSound.length + 2)) {
            currentPlayingSound = SharedResources.PlaySoundEffect(harvestSound, harvestSoundVolume);
            lastTimeAudioStarted = DateTime.Now;
        }

        curHitpoints -= _strengh;

        if(curHitpoints <= 0) {
            DestroyAndDrop();

            if(currentPlayingSound != null && currentPlayingSound.activeSelf) {
                Destroy(currentPlayingSound);
            }
        }
    }

    public void OnMouseDown() {
        if(!addedToList) {
            highlight.SetActive(true);
            RessourceManager.me.AddRessource(this);
            addedToList = true;
        }
    }

    public void Unhighlight() {
        if(addedToList) {
            highlight.SetActive(false);
            RessourceManager.me.RemoveRessource(this);
            curWorker = null;
            addedToList = false;
        }
    }

    void Awake() {
        curHitpoints = maxHitpoints;
        highlight = transform.FindChild("Highlight").gameObject;
    }

    void Destroy() {
        //Destroys object
        Destroy(gameObject);
    }

    private void DestroyAndDrop() {
        curWorker.RessourceHarvestet();
        Instantiate(itemDrop, new Vector3(this.transform.position.x, this.transform.position.y - 1, this.transform.position.z), Quaternion.identity);
        var _particle = Instantiate(particle, gameObject.transform.position, Quaternion.identity) as GameObject;
        _particle.transform.parent = gameObject.transform;
        RessourceManager.me.RemoveRessource(this);
        gameObject.transform.FindChild("Models").GetComponent<Rigidbody>().useGravity = true;
        gameObject.transform.FindChild("Models").GetComponent<Rigidbody>().isKinematic = false;
        //todo rotate random or rotate based on player position
        gameObject.transform.FindChild("Highlight").gameObject.SetActive(false);
        gameObject.transform.FindChild("Models").transform.eulerAngles = new Vector3(
            gameObject.transform.eulerAngles.x,
            gameObject.transform.eulerAngles.y,
            gameObject.transform.eulerAngles.z + 10
        );
        Invoke("Destroy", 10);
    }
}