﻿using UnityEngine;
using System.Collections;

public class HeatSource : MonoBehaviour {

    public float heatGeneration = 20;
    public float heatRange = 10;

    void Start()
    {
        TemperatureManager.me.AddHeatSource(this);
    }
}
