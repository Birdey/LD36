﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class TemperatureManager : MonoBehaviour
{
    public List<HeatSource> heatSources = new List<HeatSource>();
    public List<BasicWorker> workers = new List<BasicWorker>();

    public static TemperatureManager me;

    #region Events
    void OnEnable()
    {
        TickManager.workTick += GetTick;
    }

    void OnDisable()
    {
        TickManager.workTick -= GetTick;
    }

    void GetTick()
    {
        foreach (BasicWorker wario in workers)
        {
            float tempHeat = 0;

            foreach (HeatSource hs in heatSources)
            {
                float distance = Vector3.Distance(wario.transform.position, hs.transform.position);

                if (distance <= hs.heatRange)
                {
                    int heatGain = (int)(hs.heatGeneration - (distance/2));
                    if (heatGain <= 0)
                        heatGain = 0;
                    tempHeat += heatGain;
                }
            }

            wario.heat = (int)tempHeat;
        }
    }
    #endregion

    void Awake()
    {
        me = this;
    }

    #region Adding and Removing
    public void AddWorker(BasicWorker _worker)
    {
        workers.Add(_worker);
    }

    public void AddHeatSource(HeatSource _this)
    {
        heatSources.Add(_this);
    }

    public void RemoveHeatSource(HeatSource _this)
    {
        heatSources.Remove(_this);
    }
    #endregion

    #region Public API
    public HeatSource GetStrongestHeatSource()
    {
        HeatSource best = null;
        foreach (HeatSource hs in heatSources)
        {
            if (!best || hs.heatGeneration * hs.heatRange > best.heatGeneration * best.heatRange)
                best = hs;
        }
        return best;
    }
    #endregion
}
