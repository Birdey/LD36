﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(NavMeshAgent))]
public class Enemy : MonoBehaviour {

    public NavMeshAgent agent;

    void Start()
    {
        EnemySpawner.AddEnemy();
        GoToDestination();
    }

    void GoToDestination()
    {
        agent.SetDestination(GameObject.FindGameObjectWithTag("Campfire").transform.position);
    }

    void Die()
    {
        EnemySpawner.RemoveEnemy();
        Destroy(gameObject);
    }
}
