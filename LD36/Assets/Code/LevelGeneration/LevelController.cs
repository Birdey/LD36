﻿using UnityEngine;
using System.Collections.Generic;

public class LevelController : MonoBehaviour
{
    public int seed = 1;
    //public bool usePerlin = false;
    //public bool useBoth = true;
    public bool deleteOffScreenTiles = false;
    public int overRender = 5; //Renders this many tiles past the edge of the screen. This allows for generating tiles without the player catching it.
    public float scale = 1000; //only used for perlin
    public List<GameObject> biomes = new List<GameObject>();//prefabs to spawn as ground.
    List<CMTile> tilesToGenerate = new List<CMTile>();
    List<CMTile> generatedTiles = new List<CMTile>();
    GameObject tileContainer;
    public int needToCreateCount = 0;
    public CMBounds screenBounds = new CMBounds();
    [Range(0,1)]
    public float mergeStrength = .5f;

    float lastLeft, lastRight, lastTop, lastBottom;



    void Start()
    {
        UpdateBounds();

        tileContainer = new GameObject();
        tileContainer.name = "TileContainer";

        /*
        for (float y = screenBounds.bottom - overRender; y <= screenBounds.top + overRender; y++)
        {
            for (float x = screenBounds.left - overRender; x <= screenBounds.right + overRender; x++)
            {
                CMTile newTile = new CMTile();
                newTile.position.x = x;
                newTile.position.y = y;
                newTile.bounds = screenBounds;

                if (usePerlin)
                {
                    GenerateTilePerlin(newTile);
                }
                else
                {
                    GenerateTileRandomSeed(newTile);
                }
            }
        }*/
    }

    void Update()
    {
        UpdateBounds();
        
        //checks if camera has moved a tile or not
        if (lastLeft != screenBounds.left || lastRight != screenBounds.right || lastTop != screenBounds.top || lastBottom != screenBounds.bottom)
        {
            tilesToGenerate.Clear();
            for (float y = screenBounds.bottom - overRender; y < screenBounds.top + overRender; y++)
            {
                for (float x = screenBounds.left - overRender; x < screenBounds.right + overRender; x++)
                {
                    CMTile newTile = new CMTile();
                    newTile.position.x = x;
                    newTile.position.y = y;
                    newTile.bounds = screenBounds;

                    tilesToGenerate.Add(newTile);
                }
            }
            lastLeft = screenBounds.left;
            lastRight = screenBounds.right;
            lastTop = screenBounds.top;
            lastBottom = screenBounds.bottom;
        }

        //added cause it's faster to do this than debug. still getting WAY to many tiles to generate for some reason.
        needToCreateCount = tilesToGenerate.Count;

        //limit actually instantiating tiles to 5 per frame. This makes it where the game doesn't loose too many frames for terrain generation.
        for (int i = 0; i <= 100; i++)
        {
            if (tilesToGenerate.Count > 0)
            {
                int x = (int)tilesToGenerate[0].position.x;
                int y = (int)tilesToGenerate[0].position.y;
                int tileExists = -1;

                //check if there is already a tile generated for this one by cycling through all of the currently generated tiles.
                for (int j = 0; j < generatedTiles.Count; j++)
                {
                    CMTile oldTile = generatedTiles[j];
                    
                    if (oldTile.position.x == x && oldTile.position.y == y)
                    {
                        tileExists = j;
                    }
                }

                //if tile doesn't exist, generate tile.
                if(tileExists == -1)
                {
                    CMTile oldTile = tilesToGenerate[0];

                    //only if the tile is still on the screen
                    if (oldTile.position.x > screenBounds.left - overRender && oldTile.position.x < screenBounds.right + overRender &&
                        oldTile.position.y > screenBounds.bottom - overRender && oldTile.position.y < screenBounds.top + overRender)
                    {
                        GenerateTileBoth(oldTile);
                    }
                }
                else
                {
                    //if the tile already exists
                    i--;
                }
                tilesToGenerate.RemoveAt(0);
            }
        }

        if (deleteOffScreenTiles)
        {
            for (int j = 0; j < generatedTiles.Count; j++)
            {
                CMTile oldTile = generatedTiles[j];
                if (!(oldTile.position.x > screenBounds.left - overRender && oldTile.position.x < screenBounds.right + overRender &&
                            oldTile.position.y > screenBounds.bottom - overRender && oldTile.position.y < screenBounds.top + overRender))
                {
                    Destroy(oldTile.gameObject);
                    generatedTiles.RemoveAt(j);
                    j--;
                }
            }
        }
    }
    void GenerateTilePerlin(CMTile inputTile)
    {
        float x = inputTile.position.x;
        float y = inputTile.position.y;

        int biome = Mathf.FloorToInt(
                            (Mathf.PerlinNoise(
                                (((x) * seed) / scale),
                                (((y) * seed) / scale)
                                )
                                * biomes.Count)
                                );
        GameObject tile = (GameObject)Instantiate(biomes[biome]);
        tile.transform.position = new Vector3(x, 0, y);
        tile.name = x + "," + y;
        tile.transform.parent = tileContainer.transform;
        CMTile thisTile = new CMTile();
        thisTile.biome = biome;
        thisTile.position = new Vector2(x, y);
        thisTile.gameObject = tile;
        generatedTiles.Add(thisTile);
    }

    void GenerateTileRandomSeed(CMTile inputTile)
    {
        float x = Mathf.Floor(inputTile.position.x);
        float y = Mathf.Floor(inputTile.position.y);

        float total = 0;
        int count = 0;
        int averageDensity = 3;
        for (int j = -averageDensity; j <= averageDensity; j++)
        {
            for (int i = -averageDensity; i <= averageDensity; i++)
            {
                Random.seed = seed + Mathf.Abs(((int)x + i) * ((int)y + j));

                total += (Random.Range((int)0, biomes.Count)+.1f);
                count++;
            }
        }

        int biome = Mathf.FloorToInt(total / count);
        GameObject tile = (GameObject)Instantiate(biomes[biome]);
        tile.transform.position = new Vector3(x, 0, y);
        tile.name = x + "," + y;
        tile.transform.parent = tileContainer.transform;
        CMTile thisTile = new CMTile();
        thisTile.biome = biome;
        thisTile.position = new Vector2(x, y);
        thisTile.gameObject = tile;
        generatedTiles.Add(thisTile);
    }

    void GenerateTileBoth(CMTile inputTile)
    {
        float x = Mathf.Floor(inputTile.position.x);
        float y = Mathf.Floor(inputTile.position.y);

        float total = 0;
        int count = 0;
        int averageDensity = 10;
        for (int j = -averageDensity; j <= averageDensity; j++)
        {
            for (int i = -averageDensity; i <= averageDensity; i++)
            {
                Random.seed = seed*(int)x*(int)y;
                total += (Random.Range((int)0, biomes.Count));
                count++;
            }
        }

        int biome = Mathf.FloorToInt(total / count);
        int biome2 = Mathf.FloorToInt(
                    Mathf.PerlinNoise(
                        (((x+1000) * seed) / scale),
                        (((y+1000) * seed) / scale)
                        )
                        * biomes.Count);
        biome = Mathf.RoundToInt(((float)biome*mergeStrength) + (float)(biome2*(1-mergeStrength)));
        GameObject tile;
        try {
            tile = (GameObject)Instantiate(biomes[Mathf.Clamp(biome,0,biomes.Count)]);

            tile.transform.position = new Vector3(x, 0, y);
            tile.name = x + "," + y;
            tile.transform.parent = tileContainer.transform;
            CMTile thisTile = new CMTile();
            thisTile.biome = biome;
            thisTile.position = new Vector2(x, y);
            thisTile.gameObject = tile;
            generatedTiles.Add(thisTile);
        }
        catch(System.Exception ex)
        {
            Debug.Log("ERROR: " + biome + ", " + ex.Message);
        }
    }

    void UpdateBounds()
    {
        Vector3 bottomLeftPosition = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 60));
        Vector3 topRightPosition = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, Camera.main.pixelHeight, 60));

        screenBounds.left = Mathf.Floor(bottomLeftPosition.x);
        screenBounds.bottom = Mathf.Floor(bottomLeftPosition.z);
        screenBounds.right = Mathf.Floor(topRightPosition.x);
        screenBounds.top = Mathf.Floor(topRightPosition.z);
    }
}
public class CMBounds
{
    public float left;
    public float right;
    public float bottom;
    public float top;
}

public class CMTile
{
    public Vector2 position;
    public int biome;
    public CMBounds bounds;
    public GameObject gameObject;
}