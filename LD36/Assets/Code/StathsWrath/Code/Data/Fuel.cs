﻿using System.Collections;

using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

#endif

// Created by: StathsWrath

public class Fuel : ScriptableObject
{
    #region members

    /// <summary>
    /// total energy of the fuel
    /// </summary>
    public float totalEnergy = 20;

    /// <summary>
    /// rate at which the fule is consumed.
    /// </summary>
    public float rateOfConsumption = 1;

    public float timeToDry = 10;

    public bool isBurning;

    public float dryingMod = 1;

    public float dampTime = 5;

    private EDryness dryness = EDryness.Wet;

    public bool IsConsumed { get; protected set; }

    #endregion members

    public void ApplyEnvironmentFactor()
    {
        if (IsConsumed) return;
        // TODO: fire drying faster, rain making it wet.

        timeToDry -= Time.deltaTime * dryingMod;

        if (timeToDry <= 0)
        {
            timeToDry = 0;
            dryness = EDryness.Dry;
        }
        else if (timeToDry < dampTime)
        {
            dryness = EDryness.Damp;
        }
        else
        {
            dryness = EDryness.Wet;
        }
    }

    /// <summary>
    /// Consumes energy from the fuel source.
    /// </summary>
    /// <returns>amount of energy consumed</returns>
    public float Consume()
    {
        if (totalEnergy <= 0.1f)
        {
            isBurning = false;
            IsConsumed = true;
            return 0f;
        }

        isBurning = true;

        float energyUsed = rateOfConsumption;

        if (totalEnergy < rateOfConsumption)
        {
            energyUsed = totalEnergy;
        }

        switch (dryness)
        {
            case EDryness.Wet:
                energyUsed *= .5f;
                break;

            case EDryness.Damp:
                energyUsed *= .8f;
                break;

            case EDryness.Dry:
            default:
                break;
        }
        totalEnergy -= energyUsed;

        return energyUsed;
    }

    public enum EDryness
    {
        Wet,
        Damp,
        Dry
    }
}