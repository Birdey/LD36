﻿using System.Collections;
using UnityEngine;

public class PlaceTorch : MonoBehaviour {
    //the terrain must have a collider for this to work

    //the torch gameobject
    public GameObject torch;

    private Collider[] checkSphere;
    private RaycastHit hit;
    private Vector3 placePosition;

    public void CheckForTorch() {
        Raycast();

        if(hit.transform.tag == "torch") {
            Relight(hit.collider);
        } else {
            placePosition = hit.point + hit.normal;

            Place(torch, placePosition);
        }
    }

    void DestroyTorch() {
        Raycast();

        if(hit.transform != null && hit.transform.tag == "torch") {
            Destroy(hit.transform.gameObject);
        }
    }

    void Place(GameObject thingToPlace, Vector3 placePoz) {
        Instantiate(thingToPlace, placePoz, thingToPlace.transform.rotation);
    }

    void Raycast() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        Physics.Raycast(ray, out hit);
    }

    void Relight(Collider torchToRelight) {
        torchToRelight.GetComponent<TorchLit>().Relight();
    }

    //remove this if you want to call the place torch from another script
    void Update() {
        if(Input.GetKeyDown(KeyCode.Mouse1)) {
            CheckForTorch();
        }

        if(Input.GetKeyDown(KeyCode.Mouse0)) {
            DestroyTorch();
        }
    }
}