﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VideoTest : MonoBehaviour {
    public GameObject cube;
    WebGLMovieTexture tex;

    IEnumerator ChangeSceneAfterMovie() {
        yield return new WaitForSeconds(80);
        SceneManager.LoadScene(1);
    }

    void Start() {
        tex = new WebGLMovieTexture("StreamingAssets/FinishedVideo.mp4");
        cube.GetComponent<MeshRenderer>().material = new Material(Shader.Find("Diffuse"));
        cube.GetComponent<MeshRenderer>().material.mainTexture = tex;
        tex.Play();
        StartCoroutine(ChangeSceneAfterMovie());
    }

    void Update() {
        tex.Update();
    }
}